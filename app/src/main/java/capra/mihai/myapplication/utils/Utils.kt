package capra.mihai.myapplication.utils

import capra.mihai.myapplication.adapters.ContactsAdapter
import capra.mihai.myapplication.models.Contact

object Utils {
    fun filter(
            contactList: ArrayList<Contact>,
            editText: IdeactionEditText,
            adapter: ContactsAdapter) {
        val searchList: ArrayList<Contact> = ArrayList()
        for (item in contactList.filter { it.contactPhone.contains(editText.text.toString()) }) {
            searchList.add(Contact(
                    item.contactId,
                    item.profileImage,
                    item.contactName,
                    item.contactPhone
            ))
        }
        adapter.updateList(searchList)
    }
}