package capra.mihai.myapplication.utils

import android.content.Context
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText


class IdeactionEditText(context: Context, attr: AttributeSet)
    : EditText(context, attr) {

    override fun onKeyPreIme(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK || event.action == KeyEvent.ACTION_DOWN) {
            val manager = this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            manager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        }
        return false
    }
}

