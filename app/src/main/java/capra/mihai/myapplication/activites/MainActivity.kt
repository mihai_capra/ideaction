package capra.mihai.myapplication.activites

import android.Manifest
import android.content.ContentResolver
import android.content.ContentUris
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import capra.mihai.myapplication.R
import capra.mihai.myapplication.adapters.ContactsAdapter
import capra.mihai.myapplication.models.Contact
import capra.mihai.myapplication.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException

class MainActivity : AppCompatActivity() {

    private val PERMISSIONS_READ_CONTACTS = 102
    private lateinit var contacts: ArrayList<Contact>
    private lateinit var contactsAdapter: ContactsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkPermissionsAndLoadContacts()

        contactsRecyclerView.setHasFixedSize(true)
        contactsRecyclerView.layoutManager = LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        phoneEditText.requestFocus()
        phoneEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
            }
            true
        }
        phoneEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                //No implementation needed
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //No implementation needed
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Utils.filter(contacts, phoneEditText, contactsAdapter)
            }
        })
    }

    private fun checkPermissionsAndLoadContacts() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(
                    Manifest.permission.READ_CONTACTS),
                    PERMISSIONS_READ_CONTACTS)
        } else {
            contacts = getContacts()
            contactsAdapter = ContactsAdapter(contacts)
            contactsRecyclerView.adapter = contactsAdapter
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSIONS_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                contacts = getContacts()
            } else {
                Toast.makeText(
                        this,
                        "Permission must be granted in order to display contacts information",
                        Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
    }

    private fun getContacts(): ArrayList<Contact> {
        val tempContacts: ArrayList<Contact> = ArrayList()
        val resolver: ContentResolver = contentResolver
        val cursor = resolver.query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                null)

        if (cursor.count > 0) {
            while (cursor.moveToNext()) {
                val id = cursor.getString(
                        cursor.getColumnIndex(ContactsContract.Contacts._ID))
                val name = cursor.getString(
                        cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                val phones = (cursor.getString(
                        cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))).toInt()

                if (phones > 0) {
                    val cursorPhone = resolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                            arrayOf(id),
                            null
                    )

                    if (cursorPhone.count > 0) {
                        while (cursorPhone.moveToNext()) {
                            val phoneNumberValue = cursorPhone.getString(cursorPhone.getColumnIndex(
                                    ContactsContract.CommonDataKinds.Phone.NUMBER)
                            )
                            tempContacts.add(Contact(
                                    id.toInt(),
                                    getContactPhoto(id.toLong()),
                                    name,
                                    phoneNumberValue
                            ))
                        }
                    }
                    if (!cursorPhone.isClosed) cursorPhone.close()
                }
            }
        }
        if (!cursor.isClosed) cursor.close()
        return ArrayList(tempContacts.filter { it.contactId == it.contactId })
    }

    private fun getContactPhoto(contactId: Long): Any {
        var photo: Bitmap? = null
        try {
            val inputStream = ContactsContract.Contacts.openContactPhotoInputStream(contentResolver,
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId))
            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream)
                inputStream.close()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        if (photo == null) return R.drawable.unknown_image
        return photo
    }
}
