package capra.mihai.myapplication.adapters

import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import capra.mihai.myapplication.R
import capra.mihai.myapplication.models.Contact
import kotlinx.android.synthetic.main.contact_item.view.*

class ContactsAdapter(private var contacts: List<Contact>) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ContactsViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.contact_item, parent, false))
    }

    override fun getItemCount() = contacts.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ContactsViewHolder).bind(contacts[position])
    }

    fun updateList(searchList: List<Contact>) {
        this.contacts = searchList
        notifyDataSetChanged()
    }

    class ContactsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(contact: Contact) = with(itemView) {
            if (contact.profileImage is Bitmap) {
                itemView.contactImage.setImageBitmap(contact.profileImage)
            } else if (contact.profileImage is Int) {
                itemView.contactImage.setImageResource(contact.profileImage)
            }
            itemView.contactFullName.text = contact.contactName
            itemView.contactPhone.text = contact.contactPhone
        }
    }
}