package capra.mihai.myapplication.models

data class Contact(
        val contactId: Int,
        val profileImage: Any,
        val contactName: String,
        val contactPhone: String)